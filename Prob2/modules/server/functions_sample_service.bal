import ballerina/grpc;
import ballerina/log;


listener grpc:Listener ep = new (9090);
// local array that will store all users
cfunction [] funct =[];
sfunction [] stuf =[];

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "functions" on ep {

    remote function add_new_fn(cfunction value) returns infoResponse|error {
        log:printInfo("Creating a function");
        log:printInfo(value.'version);
        foreach var item in funct {
            if value.'version == item.'version{
                log:printError("function already exists");
            }else{
                funct.push(value);
            }
            
        }
        infoResponse res = {'version: value.'version};
        return res;       
    }
    remote function delete_fn(dfunction value) returns error? {
        foreach var item in funct {
            if item.'version == value.'version{
                // code to delete
            }else{
                log:printError("No such function found ");
            }
            
        }
    }
    remote function show_fn(string value) returns cfunction|error {
        foreach var item in funct {
            if item.hasKey(value){
                var theFunction = item;
            }else{
                log:printError("The Function is not available")//;
            }
        }
        
        
    }
    remote function add_fns(stream<cfunctionMulti, grpc:Error?> clientStream) returns infoResponse|error {
            check clientStream.forEach(isolated function(cfunctionMulti.object {}) {
            log:printInfo("Greet received: " + name);
        });

        infoResponse res = {'version: "Completed"};
        return res; 
    }
    remote function show_all_fns() returns stream<cfunction, error?>|error {
       log:printInfo("returning all functions");
       return funct.toStream();

    }
    remote function show_all_with_criteria(stream<string, grpc:Error?> clientStream) returns stream<sfunction, error?>|error {
                check clientStream.forEach(function(string chatMsg) {
                string m = chatMsg;
                foreach var item in funct {
                    if item.hasKey(m){
                    stuf.push();
                    checkpanic caller-> send sfunction.(item);
            }else{
                log:printError("The Function is not available");
            }
        }

                
        });
    }
}

