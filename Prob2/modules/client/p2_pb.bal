import ballerina/grpc;

public isolated client class functionsClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR, getDescriptorMap());
    }

    isolated remote function add_new_fn(cfunction|ContextCfunction req) returns (infoResponse|grpc:Error) {
        map<string|string[]> headers = {};
        cfunction message;
        if (req is ContextCfunction) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functions/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <infoResponse>result;
    }

    isolated remote function add_new_fnContext(cfunction|ContextCfunction req) returns (ContextInfoResponse|grpc:Error) {
        map<string|string[]> headers = {};
        cfunction message;
        if (req is ContextCfunction) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functions/add_new_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <infoResponse>result, headers: respHeaders};
    }

    isolated remote function delete_fn(dfunction|ContextDfunction req) returns (grpc:Error?) {
        map<string|string[]> headers = {};
        dfunction message;
        if (req is ContextDfunction) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functions/delete_fn", message, headers);
    }

    isolated remote function delete_fnContext(dfunction|ContextDfunction req) returns (ContextNil|grpc:Error) {
        map<string|string[]> headers = {};
        dfunction message;
        if (req is ContextDfunction) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functions/delete_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {headers: respHeaders};
    }

    isolated remote function show_fn(string|ContextString req) returns (cfunction|grpc:Error) {
        map<string|string[]> headers = {};
        string message;
        if (req is ContextString) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functions/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <cfunction>result;
    }

    isolated remote function show_fnContext(string|ContextString req) returns (ContextCfunction|grpc:Error) {
        map<string|string[]> headers = {};
        string message;
        if (req is ContextString) {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("functions/show_fn", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <cfunction>result, headers: respHeaders};
    }

    isolated remote function add_fns() returns (Add_fnsStreamingClient|grpc:Error) {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("functions/add_fns");
        return new Add_fnsStreamingClient(sClient);
    }

    isolated remote function show_all_fns() returns stream<cfunction, grpc:Error?>|grpc:Error {
        Empty message = {};
        map<string|string[]> headers = {};
        var payload = check self.grpcClient->executeServerStreaming("functions/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, _] = payload;
        CfunctionStream outputStream = new CfunctionStream(result);
        return new stream<cfunction, grpc:Error?>(outputStream);
    }

    isolated remote function show_all_fnsContext() returns ContextCfunctionStream|grpc:Error {
        Empty message = {};
        map<string|string[]> headers = {};
        var payload = check self.grpcClient->executeServerStreaming("functions/show_all_fns", message, headers);
        [stream<anydata, grpc:Error?>, map<string|string[]>] [result, respHeaders] = payload;
        CfunctionStream outputStream = new CfunctionStream(result);
        return {content: new stream<cfunction, grpc:Error?>(outputStream), headers: respHeaders};
    }

    isolated remote function show_all_with_criteria() returns (Show_all_with_criteriaStreamingClient|grpc:Error) {
        grpc:StreamingClient sClient = check self.grpcClient->executeBidirectionalStreaming("functions/show_all_with_criteria");
        return new Show_all_with_criteriaStreamingClient(sClient);
    }
}

public client class Add_fnsStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCfunctionMulti(cfunctionMulti message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCfunctionMulti(ContextCfunctionMulti message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveInfoResponse() returns infoResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return <infoResponse>payload;
        }
    }

    isolated remote function receiveContextInfoResponse() returns ContextInfoResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <infoResponse>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public class CfunctionStream {
    private stream<anydata, grpc:Error?> anydataStream;

    public isolated function init(stream<anydata, grpc:Error?> anydataStream) {
        self.anydataStream = anydataStream;
    }

    public isolated function next() returns record {|cfunction value;|}|grpc:Error? {
        var streamValue = self.anydataStream.next();
        if (streamValue is ()) {
            return streamValue;
        } else if (streamValue is grpc:Error) {
            return streamValue;
        } else {
            record {|cfunction value;|} nextRecord = {value: <cfunction>streamValue.value};
            return nextRecord;
        }
    }

    public isolated function close() returns grpc:Error? {
        return self.anydataStream.close();
    }
}

public client class Show_all_with_criteriaStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendString(string message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextString(ContextString message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveSfunction() returns sfunction|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return <sfunction>payload;
        }
    }

    isolated remote function receiveContextSfunction() returns ContextSfunction|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <sfunction>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class FunctionsCfunctionCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendCfunction(cfunction response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextCfunction(ContextCfunction response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FunctionsInfoResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendInfoResponse(infoResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextInfoResponse(ContextInfoResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FunctionsNilCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class FunctionsSfunctionCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendSfunction(sfunction response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextSfunction(ContextSfunction response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextStringStream record {|
    stream<string, error?> content;
    map<string|string[]> headers;
|};

public type ContextSfunctionStream record {|
    stream<sfunction, error?> content;
    map<string|string[]> headers;
|};

public type ContextCfunctionStream record {|
    stream<cfunction, error?> content;
    map<string|string[]> headers;
|};

public type ContextCfunctionMultiStream record {|
    stream<cfunctionMulti, error?> content;
    map<string|string[]> headers;
|};

public type ContextNil record {|
    map<string|string[]> headers;
|};

public type ContextString record {|
    string content;
    map<string|string[]> headers;
|};

public type ContextDfunction record {|
    dfunction content;
    map<string|string[]> headers;
|};

public type ContextSfunction record {|
    sfunction content;
    map<string|string[]> headers;
|};

public type ContextCfunction record {|
    cfunction content;
    map<string|string[]> headers;
|};

public type ContextInfoResponse record {|
    infoResponse content;
    map<string|string[]> headers;
|};

public type ContextCfunctionMulti record {|
    cfunctionMulti content;
    map<string|string[]> headers;
|};

public type Empty record {|
|};

public type arraytype record {|
    string body = "";
|};

public type dfunction record {|
    string 'version = "";
|};

public type sfunction record {|
    cfunction[] bycriteria = [];
|};

public type infoResponse record {|
    string 'version = "";
|};

public type cfunction record {|
    string 'version = "";
    string fname = "";
    string ftype = "";
    string freturn = "";
    string freturn_type = "";
    arraytype[] fbody = [];
    string dname = "";
    string dsurname = "";
    string demail = "";
    string flanguage = "";
|};

public type cfunctionMulti record {|
    cfunction[] allfunction = [];
|};

const string ROOT_DESCRIPTOR = "0A0870322E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F1A1B676F6F676C652F70726F746F6275662F656D7074792E70726F746F22280A0C696E666F526573706F6E736512180A0776657273696F6E180120012809520776657273696F6E221F0A0961727261797479706512120A04626F64791801200128095204626F64792298020A096366756E6374696F6E12180A0776657273696F6E180120012809520776657273696F6E12140A05666E616D651802200128095205666E616D6512140A0566747970651803200128095205667479706512180A076672657475726E18042001280952076672657475726E12210A0C6672657475726E5F74797065180520012809520B6672657475726E5479706512200A0566626F647918062003280B320A2E617272617974797065520566626F647912140A05646E616D651807200128095205646E616D65121A0A08647375726E616D651808200128095208647375726E616D6512160A0664656D61696C180920012809520664656D61696C121C0A09666C616E6775616765180A200128095209666C616E6775616765223E0A0E6366756E6374696F6E4D756C7469122C0A0B616C6C66756E6374696F6E18012003280B320A2E6366756E6374696F6E520B616C6C66756E6374696F6E22250A096466756E6374696F6E12180A0776657273696F6E180120012809520776657273696F6E22370A097366756E6374696F6E122A0A0A6279637269746572696118012003280B320A2E6366756E6374696F6E520A6279637269746572696132C5020A0966756E6374696F6E7312270A0A6164645F6E65775F666E120A2E6366756E6374696F6E1A0D2E696E666F526573706F6E7365122B0A076164645F666E73120F2E6366756E6374696F6E4D756C74691A0D2E696E666F526573706F6E73652801122F0A0964656C6574655F666E120A2E6466756E6374696F6E1A162E676F6F676C652E70726F746F6275662E456D70747912330A0773686F775F666E121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A0A2E6366756E6374696F6E12340A0C73686F775F616C6C5F666E7312162E676F6F676C652E70726F746F6275662E456D7074791A0A2E6366756E6374696F6E300112460A1673686F775F616C6C5F776974685F6372697465726961121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A0A2E7366756E6374696F6E28013001620670726F746F33";

isolated function getDescriptorMap() returns map<string> {
    return {"google/protobuf/empty.proto": "0A1B676F6F676C652F70726F746F6275662F656D7074792E70726F746F120F676F6F676C652E70726F746F62756622070A05456D70747942540A13636F6D2E676F6F676C652E70726F746F627566420A456D70747950726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "google/protobuf/wrappers.proto": "0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "p2.proto": "0A0870322E70726F746F1A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F1A1B676F6F676C652F70726F746F6275662F656D7074792E70726F746F22280A0C696E666F526573706F6E736512180A0776657273696F6E180120012809520776657273696F6E221F0A0961727261797479706512120A04626F64791801200128095204626F64792298020A096366756E6374696F6E12180A0776657273696F6E180120012809520776657273696F6E12140A05666E616D651802200128095205666E616D6512140A0566747970651803200128095205667479706512180A076672657475726E18042001280952076672657475726E12210A0C6672657475726E5F74797065180520012809520B6672657475726E5479706512200A0566626F647918062003280B320A2E617272617974797065520566626F647912140A05646E616D651807200128095205646E616D65121A0A08647375726E616D651808200128095208647375726E616D6512160A0664656D61696C180920012809520664656D61696C121C0A09666C616E6775616765180A200128095209666C616E6775616765223E0A0E6366756E6374696F6E4D756C7469122C0A0B616C6C66756E6374696F6E18012003280B320A2E6366756E6374696F6E520B616C6C66756E6374696F6E22250A096466756E6374696F6E12180A0776657273696F6E180120012809520776657273696F6E22370A097366756E6374696F6E122A0A0A6279637269746572696118012003280B320A2E6366756E6374696F6E520A6279637269746572696132C5020A0966756E6374696F6E7312270A0A6164645F6E65775F666E120A2E6366756E6374696F6E1A0D2E696E666F526573706F6E7365122B0A076164645F666E73120F2E6366756E6374696F6E4D756C74691A0D2E696E666F526573706F6E73652801122F0A0964656C6574655F666E120A2E6466756E6374696F6E1A162E676F6F676C652E70726F746F6275662E456D70747912330A0773686F775F666E121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A0A2E6366756E6374696F6E12340A0C73686F775F616C6C5F666E7312162E676F6F676C652E70726F746F6275662E456D7074791A0A2E6366756E6374696F6E300112460A1673686F775F616C6C5F776974685F6372697465726961121C2E676F6F676C652E70726F746F6275662E537472696E6756616C75651A0A2E7366756E6374696F6E28013001620670726F746F33"};
}

