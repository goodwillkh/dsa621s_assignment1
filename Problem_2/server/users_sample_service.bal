import ballerina/grpc;

listener grpc:Listener ep = new (9090);

@grpc:ServiceDescriptor {descriptor: ROOT_DESCRIPTOR, descMap: getDescriptorMap()}
service "users" on ep {

    remote function add_new_fn(cfunction value) returns infoResponse|error {
    }
    remote function delete_fn(dfunction value) returns error? {
    }
    remote function show_fn(string value) returns cfunction|error {
    }
    remote function add_fns(stream<cfunctionMulti, grpc:Error?> clientStream) returns infoResponse|error {
    }
    remote function show_all_fns() returns stream<cfunction, error?>|error {
    }
    remote function show_all_with_criteria(stream<string, grpc:Error?> clientStream) returns stream<sfunction, error?>|error {
    }
}

